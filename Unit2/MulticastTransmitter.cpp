/*
 * MulticastTransmitter.cpp
 *
 *  Created on: 09.01.2018
 *      Author: daniel
 */

#include "MulticastTransmitter.h"

#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <time.h>
#include <sys/time.h>
#include <climits>
#include <cstring>
#include <signal.h>
#include <iostream>

#include "constants.h"
#include "error_outputs.h"

#include "message.h"

MulticastTransmitter::MulticastTransmitter() {
}

MulticastTransmitter::~MulticastTransmitter()
{
	shutdown(transmissionSocket, 2);
	close(transmissionSocket);
	
}

struct addrinfo * MulticastTransmitter::createLocalAddress( char * hostname, char * portname )
{
    struct addrinfo hints;
    struct addrinfo* res=NULL;

    memset(&hints,0,sizeof(hints));
    hints.ai_family=AF_INET;
    hints.ai_socktype=SOCK_DGRAM;
    hints.ai_protocol=0;
    hints.ai_flags=AI_ADDRCONFIG;
    int err= getaddrinfo ( hostname, portname, &hints, &res);
    if (err!=0||res==NULL)
    {
        die("failed to resolve socket address (err=%d)", err );
    }
    return res;
}


int MulticastTransmitter::InitSocket(char* hostname, long port)
{
//	struct addrinfo * localAddressInfo = NULL;
	struct in_addr localInterface;
//	struct ip_mreq group;

//	struct sockaddr_in si_me, si_other;

//	socklen_t slen= sizeof(si_other);
//	socklen_t recv_len;
	int broadcastEnable=1;

//	int flags = TIMER_ABSTIME;

//	long port = strtol(portname, NULL, 10);

	//..
	/** setup socket for initial transmission to destination */
//	localAddressInfo = createLocalAddress ( hostname, portname );


	/** create transmission socket */
	transmissionSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP );

	if(transmissionSocket < 0)
	{
		dieS("failed to get socket", nullptr);
	}

	/* Initialize the group sockaddr structure */

	memset((char *) &groupSock, 0, sizeof(groupSock));
	groupSock.sin_family = AF_INET;
	groupSock.sin_addr.s_addr = inet_addr( MULTICASTGROUP );
	groupSock.sin_port = htons(port); 

/*	if(bind(transmissionSocket, (struct sockaddr*)&groupSock, sizeof(struct sockaddr_in)) < 0)
	{
		dieS("Error binding socket to interface", strerror(errno));
	}*/

	int loopch = 1;
	if ( setsockopt ( transmissionSocket, IPPROTO_IP, IP_MULTICAST_LOOP, (char *)&loopch, sizeof(loopch)) < 0)
	{
		dieS("failed to set loop count: %s", strerror(errno));
	}

#ifdef SET_IF	
	localInterface.s_addr = inet_addr( hostname );
	if(setsockopt ( transmissionSocket, IPPROTO_IP, IP_MULTICAST_IF, (char *)&localInterface, sizeof(localInterface)) < 0)
	{
		dieS("failed setting local interface address: %s ",strerror(errno));
	}
#endif
	int ttl = 250;
	if(setsockopt ( transmissionSocket, IPPROTO_IP, IP_MULTICAST_TTL, (unsigned char *)&ttl, sizeof(ttl)) < 0)
	{
		dieS("failed setting local interface address: %s ",strerror(errno));
	}

	if ( setsockopt(transmissionSocket, SOL_SOCKET, SO_BROADCAST, &broadcastEnable, sizeof(broadcastEnable)) < 0 )
	{
		dieS("failed setting broadcast enabled on transmission socket: %s", strerror(errno));
	}
	return 0;
}

int MulticastTransmitter::sendMessage(char* buffer, size_t size)
{	
    return sendto(transmissionSocket, buffer, size, 0, (struct sockaddr*)&groupSock, sizeof(struct sockaddr_in));
}


int MulticastTransmitter::sendMessage(message_t const& message)
{
  return sendto(transmissionSocket, &message, sizeof(message_t), 0, (struct sockaddr*)&groupSock, sizeof(struct sockaddr_in));
}



