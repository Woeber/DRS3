#include "Scheduler.h"

#include <time.h>
#include <iostream>

#include <sys/time.h>


#include "current_message.h"
#include "plotTime.h"
#include "constants.h"


//#define PI
#define print
#define SLOT_PRINT
#define LED     0


#ifdef PI
  #include <wiringPi.h>
#endif

using namespace std;


#define SLOTLENGTHUS 1000000
//#define SLOTLENGTHUS 1000000
#define PROTOCOLID 0x8023
#define SCHEDULELENGTH 8
#define ROUNDS 100000;

struct schedule_entity
{
    int node_id;
    int length_us;
    int is_master_node;
};

//R2-2-5
struct schedule_entity schedule_array[SCHEDULELENGTH]=
{
    {1,SLOTLENGTHUS,1},
    {2,SLOTLENGTHUS,0},
    {1,SLOTLENGTHUS,0},
    {2,SLOTLENGTHUS,0},
    {0,SLOTLENGTHUS,0},
    {1,SLOTLENGTHUS,0},
    {0,SLOTLENGTHUS,0},
    {2,SLOTLENGTHUS,0}
};

/**
 * @brief Start the scheduler
 * 
 * @return void
 */
void Scheduler::Start(int const NodeId)
{
    mNodeId = NodeId;
    /** loop a limited number of times */
    mIsRunning = true;
    
    // define the inital time
    clock_gettime(CLOCK_MONOTONIC, &mNextDeadline);
    mSchedule_index = 0;
    
//    transmitter.InitSocket();

#ifdef PI
    wiringPiSetup () ;
    pinMode (LED, OUTPUT) ;
#endif
    
    if( schedule_array[0].node_id != mNodeId ) {
      while(!last_message.NewMessageAvailable()){}
      struct timespec ReceiverTimestamp;
      message_t NewMessage = last_message.GetMessage(ReceiverTimestamp);
      cout << PlotTime(ReceiverTimestamp) << " : " << "Received new message";
      
    }
    
    //R2-2-6
    while( mIsRunning )
    {
      Run();
      //R2-2-7
      clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &mNextDeadline, NULL);
    }
}



/**
 * @brief The Run method is called at the intervalls specified in the schedule mScheduleArray.
 *        In this method the next deadline must be calculated, the multicast must be sent and 
 *        the gpio pins must be set.
 * 
 * @return void
 */
void Scheduler::Run()
{
	if(last_message.NewMessageAvailable()){
	  //Get Last Message
	  struct timespec ReceiverTimestamp;
	  message_t NewMessage = last_message.GetMessage(ReceiverTimestamp);
	 
	  //Print Last Message
	  #ifdef SLOT_PRINT
	  cout << PlotTime(ReceiverTimestamp) << " : " << "Received new message";
	  cout << "(NodeId: " << NewMessage.node_id;
	  cout << ",Timestamp: " << PlotTime(NewMessage.time);
	  cout << ",Slot: " << NewMessage.slot << ")" << endl;
	  #endif
	  
	  // Resync if the current slot is not equal to the Slot of the sync Message
	  //R2-3-5
	  //R2-3-6
	  //R2-3-7
	  //R2-3-8
	  if(	(NewMessage.slot != mSchedule_index) && 
		(schedule_array[NewMessage.slot].is_master_node) &&
		(NewMessage.protocol == MULTICAST_PROTOCOL_MAGIC_NUMBER)){
	    cout << "Slot out of sync" << endl;
	    mSchedule_index = NewMessage.slot;
	  }
	  
	  //Calculate the calibration offset so that the difference between the receive time of
	  //the message and the time now is minimised
	  /*struct timespec TimeDiff;
	  clock_gettime(CLOCK_MONOTONIC, &TimeDiff);
	  TimeDiff.tv_sec -= ReceiverTimestamp.tv_sec;
	  TimeDiff.tv_nsec -= ReceiverTimestamp.tv_nsec;
	  
	  cout << "Difference between ReceiveTimestamp and SlotTime: " << PlotTime(TimeDiff) << endl;*/
	  
	  
	  //Calculate deviation of receive time with expected receive time 
	  //R2-3-9
	  //this only works if the two hosts have the same time anyway

	  

	}
	
	//if the this mode should be active according to the table
	if(mNodeId == schedule_array[mSchedule_index].node_id){
	  //set the hardware pin
	  //R2-2-9
	  #ifdef PI
	    digitalWrite (LED, 1) ;     // On
	    usleep(10);
	    digitalWrite (LED, 0) ;     // Off
	  #endif

	  #ifdef SLOT_PRINT
	      cout << PlotTime() << " : " << "Node " << mNodeId << " active in Slot " << mSchedule_index << endl;
	  #endif  	

	      
	  //R2-2-8
	  message_t SyncMessage;
	  SyncMessage.protocol = MULTICAST_PROTOCOL_MAGIC_NUMBER;
	  SyncMessage.node_id = mNodeId;
	  SyncMessage.slot = mSchedule_index; 

	  clock_gettime(CLOCK_MONOTONIC, &SyncMessage.time);
	  mTransmitter->sendMessage(SyncMessage);
	    
	  #ifdef SLOT_PRINT
	    cout << PlotTime() << " : " << "Node " << mNodeId << " sent Sync Message in Slot " << mSchedule_index << endl;
	  #endif
	
	}
	mSchedule_index = (mSchedule_index+1)%SCHEDULELENGTH;
	
	AddToNextDeadLine(schedule_array[mSchedule_index].length_us);
	
	int CalibrationOffset_us = 0;
	AddToNextDeadLine(CalibrationOffset_us);

	//TODO   Send Message
//	transmitter.sendMessage();
}



/**
 * @brief increases / decreases the deadline
 * 
 * @param us time that is added to the deadline [us], can be negative
 * @return void
 */
void Scheduler::AddToNextDeadLine(const int us)
{
	mNextDeadline.tv_nsec += us * 1000;
	// Normalize the time to account for the second boundary
	if(mNextDeadline.tv_nsec >= 1000000000) {
	  mNextDeadline.tv_nsec -= 1000000000;
	  mNextDeadline.tv_sec++;
	}
	else if( mNextDeadline.tv_nsec < 0){
	  mNextDeadline.tv_nsec += 1000000000;
	  mNextDeadline.tv_sec--;
	}
}

