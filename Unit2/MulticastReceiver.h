#ifndef MULITCASTRECEIVER
#define MULTICASTRECEIVER

#include <memory>
#include <thread>

//R2-3-2

class MulticastReceiver{
public:
  int StartReceiver(int const ListeningPort);
  ~MulticastReceiver();
private:
  std::shared_ptr<std::thread> mThread;
  
  int InitSocket(int const ListeningPort);
  int receiveSocket= -1;
};

#endif