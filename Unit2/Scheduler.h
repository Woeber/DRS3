#ifndef SCHEDULER
#define SCHEDULER

#include <time.h>
#include <memory>
#include "MulticastTransmitter.h"



class Scheduler{
public:
  Scheduler(std::shared_ptr<MulticastTransmitter> transmitter) : mTransmitter(transmitter) {};
  void Start(int const NodeId);
  
private:
  void Run();
  
  bool   mIsRunning;
  size_t mSchedule_index;
  int mNodeId;
  
  std::shared_ptr<MulticastTransmitter> mTransmitter;

  struct timespec mNextDeadline;
  void AddToNextDeadLine(int const us);
};


#endif
