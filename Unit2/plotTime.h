#ifndef PLOTTIME
#define PLOTTIME

#include <string>
#include <sstream>
#include <time.h>
#include <stdio.h>

std::string PlotTime(const struct timespec time){
  std::stringstream ss;

  //struct tm * timeinfo;

  //timeinfo = std::localtime (&(time.tv_sec));
  //ss << "Current local time and date: " << std::asctime(timeinfo);
  
  ss << time.tv_sec << " sec " << time.tv_nsec << " nsec";
  
  return ss.str();    
}

std::string PlotTime(){
    struct timespec time;
    clock_gettime(CLOCK_MONOTONIC, &time);
    return PlotTime(time);
}

#endif