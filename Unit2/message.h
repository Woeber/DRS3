#ifndef MESSAGE_T
#define MESSAGE_T

#include <time.h>

typedef struct 
{ 
 int protocol = 0xCA57;
 int node_id;
 struct timespec time;
 int slot;
 int master;
 int membership;
 int mean_deviation;
 char data[64];
} message_t;

#endif