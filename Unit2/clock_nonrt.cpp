#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <memory>

#include "Scheduler.h"
#include "MulticastReceiver.h"
#include "current_message.h"


using namespace std;

//R2-2-1
//R2-3-1



int main( int argc, char ** argv )
{
    cout << "UDP Multicast Clock Sync Demo started" << endl;

    //-----------------------------------
    //Parse Input Argument
    //Usage: clock_nonrt NodeId ListeningPort
    // R2-2-2
    if (argc < 4) { 
	cout << "Incorrect Usage" << endl;
        cout << "Usage: clock_nonrt NodeId ListeningPort SendPort" << endl;
        return -1;
    }
    
    int NodeId = atoi(argv[1]);
    cout << "NodeId = " << NodeId << endl;
    int ListeningPort = atoi(argv[2]);
    cout << "ListeningPort = " << ListeningPort << endl;
    int SendPort = atoi(argv[3]);
    cout << "SendPort = " << SendPort << endl;
  
    //-----------------------------------
  
    cout << "Start Receiver" << endl;
    MulticastReceiver receiver;
    receiver.StartReceiver(ListeningPort);
    
    //usleep(3000);
    
    cout << "Init Transmitter" << endl;
    shared_ptr<MulticastTransmitter> transmitter = make_shared<MulticastTransmitter>();
    transmitter->InitSocket("10.0.2.15",SendPort);
   
    cout << "Start Scheduler" << endl;
      
    Scheduler scheduler(transmitter);
    scheduler.Start(NodeId);
}

