#include "MulticastReceiver.h"

#include <iostream>       // std::cout
#include <thread>         // std::thread


#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <time.h>
#include <sys/time.h>
#include <limits.h>
#include <signal.h>
#include <iostream>

#include "error_outputs.h"
#include "current_message.h"

#include "constants.h"

using namespace std;

static bool StopReceiver = false;

/**
 * @brief Creats a multicast receiver socket 
 * 
 * @return void
 */
int MulticastReceiver::InitSocket(const int ListeningPort)
{
      int reuse = 1;
      struct sockaddr_in si_me;
      struct ip_mreq group;
      struct timeval receiveSocketTimeout;
      
      /** create receive socket */
      if ((receiveSocket=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
      {
    	  dieS("failed to create receive socket",strerror(errno));
    	  return -1;
      }

      /** set resuse of port */
      if(setsockopt(receiveSocket, SOL_SOCKET, SO_REUSEADDR, (char *)&reuse, sizeof(reuse)) < 0)
      {
    	  dieS("failed setting reuse flag: %s ",strerror(errno));
    	  return -1;
      }
      
      /** setup local receive address */
      memset((char *) &si_me, 0, sizeof(si_me));
      si_me.sin_family = AF_INET;
      si_me.sin_port = htons(ListeningPort);
      si_me.sin_addr.s_addr = htonl(INADDR_ANY);
      
      
      /* bind socket to port */
      if( bind ( receiveSocket , (struct sockaddr*)&si_me, sizeof(si_me) ) == -1)
      {
		  dieS("failed to bind ping pong socket",strerror(errno));
		  return -1;
      }
      
      group.imr_multiaddr.s_addr = inet_addr( MULTICASTGROUP );
      group.imr_interface.s_addr = htonl( INADDR_ANY );
      if ( setsockopt ( receiveSocket, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *)&group, sizeof(group)) < 0)
      {
    	  dieS("failed adding multicast group address to receive socket: ",strerror(errno));
    	  return -1;
      }
      
      receiveSocketTimeout.tv_sec = 1;
      receiveSocketTimeout.tv_usec = 0;
      if ( setsockopt ( receiveSocket, SOL_SOCKET, SO_RCVTIMEO,&receiveSocketTimeout,sizeof(struct timeval)) < 0)
      {
    	  dieS("failed setting receive socket timeout: ",strerror(errno));
    	  return -1;
      }

      return 0;
}


/**
 * @brief receives messages_t on the previously initialized socket and writes them to LastMessage
 * 
 * @param receiveSocket Socket where the data is received from (must be initialized
 * @return void
 */
void ReceiverThread(int receiveSocket) 
{
   
      while ( !StopReceiver )
      {
        
	socklen_t recv_len;
	struct sockaddr_in si_other;
	socklen_t slen= sizeof(si_other);
	message_t receiveMessage;
        /** try to receive some data, this is a blocking call */
	//R2-3-3
	//R2-3-4
        if ((recv_len = recvfrom( receiveSocket, (void*)&receiveMessage, sizeof(receiveMessage), 0, (struct sockaddr *) &si_other, &slen)) == (socklen_t)-1)
        {  	    
	    if(errno == EAGAIN){
	      //cout << "No Sync Message received" << endl;
	    }
	    else{
	      dieS("failed receive data: ",strerror(errno));
	    }
        }
        else
        { 	
	    last_message.SetMessage(receiveMessage);
	}
    }
}
  
/**
   * @brief stops the receiver thread
   * 
   */
MulticastReceiver::~MulticastReceiver(){
  StopReceiver = true;    
  mThread->join();
  close(receiveSocket);
}

  
/**
   * @brief Starts the receiver thread
   * 
   * @return 0 if succeeded
   */
int MulticastReceiver::StartReceiver(int const ListeningPort){
  std::cout << "Start Receiver Thread" << std::endl;
  if(InitSocket(ListeningPort) != 0){
    cout << "Failed to create Recevier Socket" << endl;
    return -1;
  }
  mThread = make_shared<std::thread>(ReceiverThread,receiveSocket);
  return 0;
}
