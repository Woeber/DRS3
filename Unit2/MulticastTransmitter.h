/*
 * MulticastTransmitter.h
 *
 *  Created on: 09.01.2018
 *      Author: daniel
 */

#ifndef MULTICASTTRANSMITTER_H_
#define MULTICASTTRANSMITTER_H_

#include <netdb.h>
#include <mutex>
#include "message.h"

//R2-2-3

class MulticastTransmitter {
public:
	MulticastTransmitter();
	virtual ~MulticastTransmitter();

	int InitSocket(char* hostname, long port);
	int sendMessage(char* buffer, size_t size);
	int sendMessage(message_t const& message);
private:
	struct addrinfo * createLocalAddress( char * hostname, char * portname );

	int transmissionSocket = -1;
	struct sockaddr_in groupSock;	

};

#endif /* MULTICASTTRANSMITTER_H_ */
