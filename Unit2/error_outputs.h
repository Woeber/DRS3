#ifndef ERROR_OUTPUTS
#define ERROR_OUTPUTS

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define LINELEN 100

static void die(char const * const s, int i)
{
    char buffer[LINELEN];
    snprintf(buffer,LINELEN,s,i);
    perror(buffer);
    //exit(1);
}

static void dieS(char const * const s, char * e )
{
    char buffer[LINELEN];
    snprintf(buffer,LINELEN,s,e);
    perror(buffer);
   //exit(1);
}

#endif