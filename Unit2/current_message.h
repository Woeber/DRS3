#ifndef CURRENT_MESSAGE
#define CURRENT_MESSAGE

#include <thread>         // std::thread
#include <mutex>          // std::mutex

#include "message.h"


class current_message{
public:
  bool NewMessageAvailable();
  message_t GetMessage(struct timespec &timestamp);
  void SetMessage(message_t const& message);
  
  
private:
  message_t mMessage;
  struct timespec mReceivedTime;
  std::mutex mMutex;
  bool mNewMessageAvailable = false;
};

extern current_message last_message;

#endif