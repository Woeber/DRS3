#include "current_message.h"


message_t current_message::GetMessage(struct timespec &timestamp){
  mMutex.lock();
  
  mNewMessageAvailable = false;
  timestamp = mReceivedTime;
  
  message_t returnMessage;
  returnMessage = mMessage;
  
  mMutex.unlock();

  return returnMessage;
  
}
void current_message::SetMessage(message_t const& message){
  mMutex.lock();
  
  mNewMessageAvailable = true;
  mMessage = message;
  clock_gettime(CLOCK_MONOTONIC, &mReceivedTime);
  
  mMutex.unlock();
}

bool current_message::NewMessageAvailable()
{
  mMutex.lock();
  bool returnValue =  mNewMessageAvailable;
  mMutex.unlock();
  return returnValue;
}


current_message last_message;