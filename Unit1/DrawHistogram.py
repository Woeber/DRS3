#!/usr/bin/env python
import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import csv

with open('./Roundtrip_Times_normal.csv', "r") as ifile:

    read = csv.reader(ifile,delimiter=';', quoting=csv.QUOTE_NONNUMERIC)
    data = list()
    for row in read :
        data += row;
    
    # the histogram of the data
	# R1-4-6 ... 50us bins lead to a very compressed histogramm due to many outliers ( logarithmic x axis & Freedman Diaconis bins estimator)
    n, bins, patches = plt.hist(data, bins='fd', normed=0)#, log=1)
    # R1-4-7 ???
    plt.xlabel('Roundtrip Time')
    plt.ylabel('Occurances')
    plt.title('Distribution of Roundtrip Times')
    #plt.axis([40, 160, 0, 0.03])
    plt.grid(True)
    plt.xscale('log', nonposy='clip')

    plt.show()
    
    max_value = max(data)
    min_value = min(data)
    avg_value = sum(data)/len(data)
    # R1-4-3
    print("Max. Roundtrip Time:", max_value)
	# R1-4-4
    print("Min. Roundtrip Time:", min_value) 
	# R1-4-5
    print("Avg. Roundtrip Time:", avg_value) 
