/*M**********************************************************************
* FILENAME :        socket_single.c             DESIGN REF: 
*
* DESCRIPTION :
*	Transmits an initial UDP message and echoes all messages it receives 
*   The IP Adress/Port for the initial message are the first two parameters
*	The Port the echo function should listen to is the third parameter  
*	The times [us] it takes from echoing a message and receiving another
*	are logged into a csv file
* 
* AUTHOR :    Illk Daniel / Woeber Johannes       
*
* CHANGES :
*
*M*/

/** R1-3-1 */

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <time.h>
#include <sys/time.h>
#include <limits.h>
#include <signal.h>
#include <iostream>


#include <vector>

#include <fstream>
#include <iterator>


#define BUFLEN 512  //Max length of buffer
#define PORT 10000   //The port on which to listen for incoming data
#define PORT_STRING "10000"
#define LINELEN 80
#define ROUNDS 100000;


#define print

using namespace std;
//---------------------------------------------------------------------------------------------------------------
void die(char const * const s, int i)
{
    char buffer[LINELEN];
    snprintf(buffer,LINELEN,s,i);
    perror(buffer);
    exit(1);
}
//---------------------------------------------------------------------------------------------------------------
void dieS(char const * const s, char * e )
{
    char buffer[LINELEN];
    snprintf(buffer,LINELEN,s,e);
    perror(buffer);
    exit(1);
}
//---------------------------------------------------------------------------------------------------------------
struct addrinfo * createDestinationHostname( char const * const hostname, char const * const portname )
{
    struct addrinfo hints;
    struct addrinfo* res=NULL;

    memset(&hints,0,sizeof(hints));
    hints.ai_family=AF_INET;
    hints.ai_socktype=SOCK_DGRAM;
    hints.ai_protocol=0;
    hints.ai_flags=AI_ADDRCONFIG;
    int err= getaddrinfo ( hostname, portname, &hints, &res);
    if (err!=0||res==NULL)
    {
        die("failed to resolve remote socket address (err=%d)", err );
    }
    return res;
}

//---------------------------------------------------------------------------------------------------------------
//returns the system time in us
long GetTime(void){
  struct timeval timestamp;
  /** R1-4-2 */
  gettimeofday(&timestamp, NULL);
  return (timestamp.tv_sec * 1000000 + timestamp.tv_usec);
}
//---------------------------------------------------------------------------------------------------------------
//writes the passed data into a csv file
int WriteCSV(vector<long> const& data, string const& filename){
  ofstream file(filename.c_str(), ofstream::out);
  file  << *data.begin() ;
  for ( auto i = data.begin()+1; i != data.end(); i++ ) {
    file << ";" << *i ;
  }
  file.close();
  return 0;
}
//---------------------------------------------------------------------------------------------------------------



int main( int argc, char ** argv )
{
    struct sockaddr_in si_me, si_other;
    struct addrinfo * destinationAddressInfo= NULL;
    int initialTransmissionSocket= -1;
    int pingpongTransmissionSocket= -1;
    socklen_t slen= sizeof(si_other);
    socklen_t recv_len;
    int s, i;
    char buf[BUFLEN];
    struct timeval start, end;
    int is_start_set= 0;
    long port = PORT;
    string InitMessagePort(PORT_STRING);
	
    /** R1-3-2 */
	//./socket_single.out target_ip_address (opt)target_port (opt)listening_port
    if((argc < 2) || (argc > 4)){
      printf("too few arguments. Correct useage socket_single target_ip_address (opt)target_port (opt)listening_port\n");
      return -1;
    } 
    else if(argc == 3){
      InitMessagePort.assign(argv[2]);
    }
    else if(argc == 4){
      InitMessagePort.assign(argv[2]);
      port = stol( argv[3] );
      printf("Open alternative Port : %d \n",port);
    }
    
    long rounds= ROUNDS;

    /** setup socket for initial transmission to destination */
    destinationAddressInfo= createDestinationHostname ( argv[1], InitMessagePort.c_str() );


    /* create a UDP socket for ping-pong transmission */
	/** R1-3-3 R1-3-4 */
    if ((pingpongTransmissionSocket=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    {
        dieS("failed to create ping pong socket",strerror(errno));
    }



    /** setup local receive address */
    memset((char *) &si_me, 0, sizeof(si_me));
    si_me.sin_family = AF_INET;
    si_me.sin_port = htons(port);
    si_me.sin_addr.s_addr = htonl(INADDR_ANY);




    /* bind socket to port */
	/** R1-3-5 */
    if( bind ( pingpongTransmissionSocket , (struct sockaddr*)&si_me, sizeof(si_me) ) == -1)
    {
        dieS("failed to bind ping pong socket",strerror(errno));
    }

	//contains all roundtrip times
    vector<long> roundtrip_times;

    /* send out initial package */
	/** R1-3-7 */
    if (sendto(pingpongTransmissionSocket,"hi",2,0, destinationAddressInfo->ai_addr,destinationAddressInfo->ai_addrlen)==-1)
    {
        dieS("failed sendto initial transmission ",strerror(errno));
    }
    long timestamp_sent,timestamp_receive;
    timestamp_sent = GetTime();

    #ifdef print
      printf("Sent inital packet to %s:%d\n", argv[1], ntohs(pingpongTransmissionSocket));
      printf("%ld\n", timestamp_sent);
      printf("Data: %s\n\n\n" , "hi");
    #endif


    while( rounds > 0 )
    {
	rounds--;
        long time_difference_us= 0;

        //---------------------------------------------------------------------------------------------------------------
        //try to receive some data, this is a blocking call
        if ((recv_len = recvfrom( pingpongTransmissionSocket, buf, BUFLEN, 0, (struct sockaddr *) &si_other, &slen)) == -1)
        {
            dieS("failed to recvfrom ",strerror(errno));
        }
        timestamp_receive = GetTime();

		#ifdef print
			//print the time
			printf("Received packet from %s:%d\n", inet_ntoa(si_other.sin_addr), ntohs(si_other.sin_port));
			printf("%ld\n", timestamp_receive-timestamp_sent);


			printf("Data: %s\n\n\n" , buf);
		#endif
		
		/** R1-4-1 */
		roundtrip_times.push_back( timestamp_receive-timestamp_sent);
		
        //----------------------------------------------------------------------------------------------------------------
        // echo the data
        is_start_set= 1;
		/** R1-3-6 */
        if (sendto( pingpongTransmissionSocket, buf, recv_len, 0, (struct sockaddr*) &si_other, slen) == -1)
        {
            dieS("failed to sendto ",strerror(errno));
        }

		timestamp_sent = GetTime();

		#ifdef print
			//print the time
			printf("Echoed packet to %s:%d\n", inet_ntoa(si_other.sin_addr), ntohs(si_other.sin_port));

			printf("Data: %s\n\n\n" , buf);
		#endif

	//----------------------------------------------------------------------------------------------------------------------------------
    }

    WriteCSV(roundtrip_times,"Roundtrip_Times.csv");


    freeaddrinfo(destinationAddressInfo);
    close(initialTransmissionSocket);
    close(pingpongTransmissionSocket);
    return 0;
}
